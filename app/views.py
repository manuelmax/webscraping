from django.shortcuts import render

from django.http import HttpResponse

import requests
from bs4 import BeautifulSoup as BS
from app.models import Page
# Create your views here.
def spider(request):
    sms = ""

    pages = Page.objects.all()
    for p in pages:
        page = requests.get(p.url)

        if page.status_code == 200:
            content = page.content
            html = BS(content, 'html.parser')
            div_noticias = html.find('div',{'class':'section-recent-list'}) 
            h2 = div_noticias.find_all('h2', {'class':'abstract-title'})
            links = [h.find('a') for h in h2]

            sms = "{} links encontrados <br>".format(len(links))
            for l in links:
                sms += '{titulo} <br> {link} <br>'.format(titulo=l.getText(), link=l.get('href'))
        else:
            sms = "Not ready!"
    

    return HttpResponse(sms)
