from django.db import models

# Create your models here.
class Page(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField(max_length=255)

    class Meta:
        db_table = "page"

    def __str__(self):
        return self.name
#end file models.py
